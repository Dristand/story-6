from django.db import models
from django.forms import ModelForm

# Create your models here.
class Event(models.Model):
    name = models.CharField('Event Name', max_length=25)
    deskripsi = models.TextField('Description', blank=True)

    def __str__(self):
        return self.name

class Member(models.Model):
    name = models.CharField('Name', max_length=25)
    event = models.ForeignKey('Event', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name


class CreateEvent(ModelForm):
    class Meta:
        model = Event
        fields = ['name', 'deskripsi']

class CreateMember(ModelForm):
    class Meta:
        model = Member
        fields = ['name']