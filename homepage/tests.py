from django.test import TestCase, Client
from django.urls import resolve
from .views import index, addEvent, addMember
from .models import Event, Member

# Create your tests here.
class TestIndex(TestCase):
	def test_index_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_index_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_index_using_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

class TestAddEvent(TestCase):
	def test_add_Event_url_is_exist(self):
		response = Client().get('/addEvent/')
		self.assertEqual(response.status_code, 200)

	def test_add_Event_index_func(self):
		found = resolve('/addEvent/')
		self.assertEqual(found.func, addEvent)

	def test_add_Event_using_template(self):
		response = Client().get('/addEvent/')
		self.assertTemplateUsed(response, 'addEvent.html')

	def test_Event_model_create_new_object(self):
		acara = Event(name ="abc")
		acara.save()
		self.assertEqual(Event.objects.all().count(), 1)

class TestAddMember(TestCase):
	def setUp(self):
		acara = Event(name ="abc")
		acara.save()

	def test_add_Member_url_is_exist(self):
		response = Client().post('/addMember/1', data={'nama':'jeremy'})
		self.assertEqual(response.status_code, 200)

class TestDeleteMember(TestCase):
	def setUp(self):
		event = Event.objects.create(name='pepewe')
		member = Member.objects.create(name="jeremy", event=event)

	def test_delete_Member_url_is_exist(self):
		response = Client().get('/deleteMember/1')
		self.assertEqual(response.status_code, 302)

class TestModel(TestCase):
	def test_str_model_event(self):
		event = Event.objects.create(name='pepewe')
		self.assertEqual(event.__str__(), 'pepewe')

	def test_str_model_member(self):
		event = Event.objects.create(name='pepewe')
		member = Member.objects.create(name="memberTest", event=event)
		self.assertEqual(member.__str__(), 'memberTest')
