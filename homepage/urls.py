from django.urls import path
from . import views

app_name = "homepage"

urlpatterns = [
    path('', views.index, name='index'),
    path('addEvent/', views.addEvent, name='addEvent'),
    path('addMember/<int:id>', views.addMember, name='addMember'),
    path('deleteMember/<int:id>', views.deleteMember, name='deleteMember'),
]
