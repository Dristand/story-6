from django.shortcuts import render, redirect
from .models import Event, Member, CreateEvent, CreateMember

# Create your views here.
def index(request):
    event = Event.objects.all()
    member = Member.objects.all()

    return render(request, 'index.html', {'events' : event, "member" : member})

def addEvent(request):
    if request.method == 'POST':
        form = CreateEvent(request.POST)

        if form.is_valid():
            form.save()
            return redirect('homepage:index')
    else:
        form = CreateEvent()

    return render(request, 'addEvent.html', {'form' : form})


def addMember(request, id):

    if request.method == 'POST':
        form = CreateMember(request.POST)

        if form.is_valid():
            form = Member(event = Event.objects.get(id = id), name = form.data['name'])
            form.save()
            return redirect('homepage:index')
    else:
        form = CreateMember()
            
    return render(request, 'addMember.html', {'form': form})

def deleteMember(request, id):

    buck = Member.objects.get(id = id)
    buck.delete()

    return redirect('homepage:index')
